import tensorflow as tf
import numpy as np
from sklearn import preprocessing

DATAFILE='./data/abalone.data'


tf.app.flags.DEFINE_integer('n_hidden_1', 64,
                            """1st layer number of features.""")
tf.app.flags.DEFINE_integer('n_hidden_2', 64,
                            """2nd layer number of features.""")
tf.app.flags.DEFINE_integer('n_input', 11,
                            """data input size""")
tf.app.flags.DEFINE_integer('n_classes', 8,
                            """number of outputs""")

FLAGS = tf.app.flags.FLAGS


def get_variables():
    with open('vars.list','r') as f:
        return([x for x in f.read().split('\n') if x!=''])

def _newvar(shape,name):
    return tf.Variable(tf.random_normal(shape, stddev=0.1),name=name)

def _newDense(input,scope_name,nout,act=tf.nn.sigmoid):
    with tf.name_scope(scope_name) as scope:
        w = _newvar([input.get_shape().as_list()[1],nout],'w')
        b = _newvar([nout,],'b')
        out=tf.add(tf.matmul(input,w),b)
        if act is not None:
            out = act(out)
        return(out)


def get_model(x,sess):
    l1=_newDense( x,'l1', FLAGS.n_hidden_1, act=tf.nn.relu)
    l2=_newDense(l1,'l2', FLAGS.n_hidden_2, act=tf.nn.relu)
    l3=_newDense(l2,'l3', int(FLAGS.n_hidden_2/2), act=tf.nn.relu)
    l4=_newDense(l3,'l4', FLAGS.n_classes, act=None)
    model=l4;
    saver = tf.train.Saver(tf.global_variables())
    ckpt = tf.train.get_checkpoint_state('./chk/')
    if ckpt and ckpt.model_checkpoint_path:
        print('Restored from checkpoint')
        saver.restore(sess, ckpt.model_checkpoint_path)

    return(model)

def get_new_model(x,sess):
    l1=_newDense( x,'l1', FLAGS.n_hidden_1-48, act=tf.nn.relu)
    l2=_newDense(l1,'l2', FLAGS.n_hidden_2, act=tf.nn.relu)
    l3=_newDense(l2,'l3', int(FLAGS.n_hidden_2/2), act=tf.nn.relu)
    l4=_newDense(l3,'l4', FLAGS.n_classes, act=None)
    model=l4;
    return(model)

def get_train():
   X,Y=_read_abalone()
   X=X[:3133,:]
   Y=Y[:3133,:]
   return(X,Y)


def get_test():
#    predict_op = tf.argmax(py_x, 1)
    X,Y=_read_abalone()
    X=X[-1044:,:]
    Y=Y[-1044:,:]
    return(X,Y)

def _setClass_onehot(rings):
    if (rings <=6):
        return([1,0,0,0,0,0,0,0])
    if (rings==7):
        return([0,1,0,0,0,0,0,0])
    if (rings==8):
        return([0,0,1,0,0,0,0,0])
    if (rings==9):
        return([0,0,0,1,0,0,0,0])
    if (rings==10):
        return([0,0,0,0,1,0,0,0])
    if (rings==11):
        return([0,0,0,0,0,1,0,0])
    if (rings==12) or (rings==13):
        return([0,0,0,0,0,0,1,0])
    if (rings >= 14):
        return([0,0,0,0,0,0,0,1])
    print('wrong ring number!')
    exit(-1)


def _IMF_onehot(IMF):
    if (IMF=='M'):
        return([1,0,0])
    if (IMF=='I'):
        return([0,1,0])
    if (IMF=='F'):
        return([0,0,1])
    print('SUPPOSED TO BE I M F !')
    exit(-1)

def _read_abalone():
    data=[]
    y=[]
    with open(DATAFILE,'r') as f:
        for line in f:
            vals=line.split(',')
            cdata=_IMF_onehot(vals[0])
            cdata.extend([float(x) for x in vals[1:-1]])
            data.append(cdata)
            y.append(_setClass_onehot(float(vals[-1])))
    return((preprocessing.scale(np.array(data)),np.array(y)))
