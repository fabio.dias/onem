from model import get_model, get_train, get_test
import tensorflow as tf



dx,dy=get_train()
tx,ty=get_test()

with tf.name_scope('input') as scope:
    x = tf.placeholder("float", [None, dx.shape[1]],name='x')
    y = tf.placeholder("float", [None, dy.shape[1]],name='y')


config = tf.ConfigProto()
config.gpu_options.allow_growth = True

with tf.Session(config=config) as sess:
    model=get_model(x,sess)

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=model, labels=y))
    learning_rate = 0.001
    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    #ckpt = tf.train.get_checkpoint_state('./chk/')
    #if ckpt and ckpt.model_checkpoint_path:
    #    print('Restored from last checkpoint')
    #    saver.restore(sess, ckpt.model_checkpoint_path)

    correct_prediction = tf.equal(tf.argmax(model, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    saver = tf.train.Saver(tf.global_variables())
    summary_writer = tf.summary.FileWriter('./chk/', sess.graph)

    summaries = tf.get_collection(tf.GraphKeys.SUMMARIES)
    for var in tf.trainable_variables():
        summaries.append(tf.summary.histogram(var.op.name, var))

    summary_op = tf.summary.merge(summaries)
    tf.global_variables_initializer().run()
    
    for step in range(100000):
        sess.run(train_op, feed_dict={x: dx, y: dy})
        if (step % 10000) == 0:
            print("Accuracy: {0}".format(accuracy.eval({x: tx, y: ty})))
        if (step % 1000) == 0:
            summary_str = sess.run(summary_op)
            summary_writer.add_summary(summary_str, step)
            saver.save(sess, './chk/', global_step=step)

#        if (i % 100) == 0:
#            print(tf.argmax(model,1).eval({x:dx[:1,:]}))
#            print(tf.argmax(y,1).eval({y:dy[:1,:]}))
