import tensorflow as tf
from model import get_model


with tf.name_scope('input') as scope:
#    x = tf.placeholder("float", [None, tx.shape[1]],name='x')
    x = tf.placeholder(tf.float32, [None, 224, 224, 3], name='x')

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
with tf.Session(config=config) as sess:
    model=get_model(x,sess)
    todo=[model,]
    layers={}
    while todo:
        cNode=todo.pop()
        # finding the variables
        clinks=list(cNode.op.inputs)
        if len(clinks)==0 and '/' in cNode.op.name:
            vals=cNode.op.name.split('/')
            if (vals[0] not in layers):
                layers[vals[0]]=[cNode.op.name,]
            else:
                layers[vals[0]].append(cNode.op.name)
        else:
            todo.extend(clinks)

    print(layers)
