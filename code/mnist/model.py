import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np



tf.app.flags.DEFINE_integer('n_hidden_1', 256,
                            """1st layer number of features.""")
tf.app.flags.DEFINE_integer('n_hidden_2', 256,
                            """2nd layer number of features.""")
tf.app.flags.DEFINE_integer('n_input', 784,
                            """MNIST data input (img shape: 28*28)""")
tf.app.flags.DEFINE_integer('n_classes', 10,
                            """MNIST total classes (0-9 digits)""")

FLAGS = tf.app.flags.FLAGS

def get_variables():
    with open('vars.list','r') as f:
        return([x for x in f.read().split('\n') if x!=''])

def _getmnist():
    return(input_data.read_data_sets("/tmp/data/", one_hot=True))

def get_test():
    mnist=_getmnist()
    X=mnist.test.images.copy()
    Y=mnist.test.labels.copy()
    mask=[2,4,6,8]
    for d in mask:
        digits=np.argmax(Y,axis=1)
        X=X[digits!=d,:]
        Y=Y[digits!=d,:]
    return(X,Y)

def get_train():
    mnist=_getmnist()
    return(mnist.train)

def _newvar(shape,name):
    return tf.Variable(tf.random_normal(shape, stddev=0.1),name=name)

def _newDense(input,scope_name,nout,act=tf.nn.sigmoid):
    with tf.name_scope(scope_name) as scope:
        w = _newvar([input.get_shape().as_list()[1],nout],'w')
        b = _newvar([nout],'b')
        out=tf.add(tf.matmul(input,w),b)
        if (act):
            out = act(out)
        return(out)


# Create model
def get_model(x, sess):
    l1=_newDense(x,'l1',FLAGS.n_hidden_1,tf.nn.relu)
    l2=_newDense(l1,'l2',FLAGS.n_hidden_2,tf.nn.relu)
    l3=_newDense(l2,'l3',FLAGS.n_hidden_2,tf.nn.relu)
    model=_newDense(l3,'out',FLAGS.n_classes,None)
    saver = tf.train.Saver(tf.global_variables())
    ckpt = tf.train.get_checkpoint_state('./chk/')
    if ckpt and ckpt.model_checkpoint_path:
        print('Restored from checkpoint')
        saver.restore(sess, ckpt.model_checkpoint_path)

#    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=l3, labels=y))
    return(model)
