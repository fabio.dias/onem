'''
A Multilayer Perceptron implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits
(http://yann.lecun.com/exdb/mnist/)

Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''
from model import get_model, get_train, get_test

# Import MNIST data
import tensorflow as tf


def _chk(step):
    summary_str = sess.run(summary_op)
    summary_writer.add_summary(summary_str, step)
    saver.save(sess, './chk/', global_step=step)


# Parameters
learning_rate = 0.001
training_epochs = 15
batch_size = 100
display_step = 1

FLAGS = tf.app.flags.FLAGS

# tf Graph input
with tf.name_scope('input') as scope:
    x = tf.placeholder("float", [None, FLAGS.n_input],name='x')
    y = tf.placeholder("float", [None, FLAGS.n_classes],name='y')


# Launch the graph
config = tf.ConfigProto()
config.gpu_options.allow_growth = True

train=get_train()


with tf.Session(config=config) as sess:
    # Construct model
    pred = get_model(x,sess)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
    # Define loss and optimizer

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    # Initializing the variables
    init = tf.global_variables_initializer()

    sess.run(init)
    saver = tf.train.Saver(tf.global_variables())
    summary_writer = tf.summary.FileWriter('./chk/', sess.graph)

    summaries = tf.get_collection(tf.GraphKeys.SUMMARIES)
    for var in tf.trainable_variables():
        summaries.append(tf.summary.histogram(var.op.name, var))

    summary_op = tf.summary.merge(summaries)

    # Training cycle
    for epoch in range(training_epochs):
        avg_cost = 0.
        total_batch = int(train.num_examples/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_x, batch_y = train.next_batch(batch_size)
            # Run optimization op (backprop) and cost op (to get loss value)
            _,c = sess.run([optimizer,cost], feed_dict={x: batch_x, y: batch_y})
            # Compute average loss
            avg_cost += c / total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", \
                "{:.9f}".format(avg_cost))
            _chk(epoch)
    _chk(training_epochs)

    print("Optimization Finished!")

    # Test model
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    tx,ty=get_test()
    print("Accuracy:", accuracy.eval({x: tx, y: ty}))
