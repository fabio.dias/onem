import tensorflow as tf
from model import get_model, get_new_model, get_test, get_variables
import networkx as nx
import matplotlib.pylab as plt
import numpy as np
import json

tx,ty=get_test()
with tf.name_scope('input') as scope:
    x = tf.placeholder("float", [None, tx.shape[1]],name='x')
#    x = tf.placeholder(tf.float32, [None, 224, 224, 3], name='x')
    y = tf.placeholder("float", [None, ty.shape[1]],name='y')

config = tf.ConfigProto()#log_device_placement=True)
config.gpu_options.allow_growth = True
with tf.Session(config=config) as sess:
    tf.global_variables_initializer().run()
    oldModel=get_model(x,sess)

    correct_prediction = tf.equal(tf.argmax(oldModel, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print(accuracy.eval({x: tx, y: ty}))

    #nx graph as model
    G=nx.DiGraph()
    todo=[oldModel,]
    layers={}
    while todo:
        cNode=todo.pop()
        clinks=list(cNode.op.inputs)
        for n in clinks:
            G.add_edge(n.name,cNode.name)
        todo.extend(clinks)

    graph=tf.get_default_graph()
    for g in G.nodes():
        cT=graph.get_tensor_by_name(g)
        G.node[g]['optype']=cT.op.type
        G.node[g]['shape']=cT.shape.as_list()
        G.node[g]['value']=cT.eval({x:tx})

# pos=nx.spring_layout(G)
# nx.draw(G,pos=pos)
# labels=nx.draw_networkx_labels(G,pos=pos)


# json data guiding the removal
with open('data.json','r') as f:
    data=json.loads(f.read())

removeOrder=dict()
for i in range(len(data['scopes'])):
    removeOrder[data['scopes'][i]['name']]=data['scopes'][i]['removeOrder']
del(data)

# new model
tf.reset_default_graph()
with tf.name_scope('input') as scope:
    x = tf.placeholder("float", [None, tx.shape[1]],name='x')
#    x = tf.placeholder(tf.float32, [None, 224, 224, 3], name='x')
    y = tf.placeholder("float", [None, ty.shape[1]],name='y')

with tf.Session(config=config) as sess:
    newModel=get_new_model(x,sess)
#    tf.global_variables_initializer().run()
    W=dict()
    upOps=dict()
    for v in G.nodes():#[x for x in G.nodes() if len(G.predecessors(x))==0]: #only "end" vars
        if ('ariable' not in G.node[v]['optype']):
            continue
        W[v]=tf.get_default_graph().get_tensor_by_name(v)
        cV=G.node[v]['value'].copy()
        if (np.any(cV.shape!=W[v].shape)):
            if (len(W[v].shape)==2):
                if (W[v].shape[0]!=cV.shape[0]):
                    nRemove=cV.shape[0]-W[v].get_shape().as_list()[0]
                    toremove=sorted(removeOrder[v][:nRemove],reverse=True)
                    for i in toremove:
                        cV=np.delete(cV,i,axis=0)
                #IMPORTANT! to understand this, PLOT THE GRAPH!
                else:# prev W
                    cN=v #this logic might be a problem if you don't use _newDense
                    while ('MatMul' not in cN):
                        cN=G.successors(cN)[0] #the first mathmul is wx+b for this var
                    cN=G.successors(cN)[0]  #gotta climb higher
                    while ('MatMul' not in cN):
                        cN=G.successors(cN)[0]

                    target=G.predecessors([x for x in G.predecessors(cN) if 'read' in x][0])[0]
                    nRemove=cV.shape[1]-W[v].get_shape().as_list()[1]
                    toremove=sorted(removeOrder[target][:nRemove],reverse=True)
                    for i in toremove:
                        cV=np.delete(cV,i,axis=1)


            if (len(W[v].shape)==1): #+b
                cN=v #this logic might be a problem if you don't use _newDense
                while ('MatMul' not in cN):
                    cN=G.successors(cN)[0]
                target=G.predecessors([x for x in G.predecessors(cN) if 'read' in x][0])[0]
                nRemove=cV.shape[0]-W[v].get_shape().as_list()[0]
                toremove=sorted(removeOrder[target][:nRemove],reverse=True)
                for i in toremove:
                    cV=np.delete(cV,i,axis=0)

            upOps[v] = tf.assign(W[v],cV)
        else:
            upOps[v] = tf.assign(W[v],G.node[v]['value'])

    for v in upOps:
        sess.run(upOps[v])

    correct_prediction = tf.equal(tf.argmax(newModel, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print(accuracy.eval({x: tx, y: ty}))
