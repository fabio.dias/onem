from model import get_model, get_test, get_variables
import tensorflow as tf
import numpy as np
import json
import matplotlib.pylab as plt
from time import time

def getScope(graphNode):
    if ('/' not in graphNode.op.name):
        return('')
    return(graphNode.op.name.split('/')[0])

def testVI(W,upPH,upOp,tx,ty,i,resetVal=True):
    origW=W.eval()
    cW=origW.copy()
    cW[i,:]=0
    sess.run(upOp, {upPH: cW})
    acc=accuracy.eval({x: tx, y: ty})
    if (resetVal):
        sess.run(upOp, {upPH: origW})
    return(acc)

def runSingleVariable(W,upPH,upOp,v,tx,ty,ignMask=[]):
    acc=np.zeros([W.shape[0],])
    todo=list(set(range(W.shape[0]))-set(ignMask))
    for i in todo:
        #t0=time()
        acc[i]=testVI(W,upPH,upOp,tx,ty,i)
        #print('ETA {0}'.format(((time()-t0)*len(todo))/60))
    return(acc)

def runCummVariable(W,upPH,upOp,v,tx,ty):
    cumm=np.zeros([W.shape[0]-1,])
    origW=W.eval()
    done=[]

    for i in range(W.shape[0]-1):
        acc=runSingleVariable(W,upPH,upOp,v,tx,ty,done)
        nextInd=np.argsort(acc)[-1]
        cumm[i]=testVI(W,upPH,upOp,tx,ty,nextInd,resetVal=False)
        done.append(nextInd)
    #makes sure we keep the original network for later
    sess.run(upOp, {upPH: origW})
    return(cumm,done)



tx,ty=get_test()
with tf.name_scope('input') as scope:
    # pick the correct placeholder for your application
    x = tf.placeholder("float", [None, tx.shape[1]],name='x')
#    x = tf.placeholder(tf.float32, [None, 224, 224, 3], name='x')
    y = tf.placeholder("float", [None, ty.shape[1]],name='y')

config = tf.ConfigProto()#log_device_placement=True)
config.gpu_options.allow_growth = True
with tf.Session(config=config) as sess:
    tf.global_variables_initializer().run()
    model=get_model(x,sess)
    variables=get_variables()
    scopes=[x.split('/')[0] for x in variables]

    correct_prediction = tf.equal(tf.argmax(model, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    trainedAcc=accuracy.eval({x: tx, y: ty})

    accuracies=dict()
    cummulative=dict()
    removeOrder=dict()
    #if you use assign directly, it creates new tensors for each call
    # caveat is to use placeholders and one update with assign
    upOps=dict()
    upPH=dict()
    W=dict()
    for v in variables:
        print('processing variable '+v)
        W[v]=tf.get_default_graph().get_tensor_by_name(v)
        print(W[v].get_shape())
        upPH[v]= tf.placeholder(W[v].dtype, shape=W[v].get_shape())
        upOps[v] = tf.assign(W[v],upPH[v])
        accuracies[v]=runSingleVariable(W[v],upPH[v],upOps[v],v,tx,ty)
        cummulative[v],removeOrder[v]=runCummVariable(W[v],upPH[v],upOps[v],v,tx,ty)

    accAll=[]
    for v in accuracies:
        accAll.extend([{'i':i,'value':accuracies[v][i],'v':v} for i in range(len(accuracies[v]))])

        #process the output layer as well
        # preds=np.argmax(sess.run(model,{x:tx}),axis=1)
        # correct=np.argmax(ty,axis=1)
        # modelScope='output'
        # accuracies[modelScope]=np.zeros([ty.shape[1],])
        # cummulative[modelScope]=np.zeros([ty.shape[1]+1,])
        # cummulative[modelScope][0]=trainedAcc
        # for i in range(ty.shape[1]):
        #     cp=preds.copy()
        #     cp[np.where(cp==i)]=-1
        #     accuracies[modelScope][i]=np.sum(np.equal(cp,correct))/len(preds)
        #     #accAll.append({'v':modelScope,'i':i,'value':accuracies[modelScope][i]})
        # wOrder=list(np.argsort(accuracies[modelScope]))
        # cp=preds.copy()
        # for i in range(len(wOrder)):
        #     cp[np.where(cp==wOrder[i])]=-1
        #     cummulative[modelScope][i+1]=np.sum(np.equal(cp,correct))/len(preds)


        #Consider nodes from all layers for a new cummulative graph
        # cumAll=np.zeros(len(accAll))+1
        # cumAll[0]=trainedAcc
        # aaSorted=sorted(accAll,key=lambda x:x['value'],reverse=True)
        # ind=0
        # for k in range(len(accAll)-1):
        #     v=aaSorted[k]['v']
        #     i=aaSorted[k]['i']
        #     cW=W[v].eval()
        #     cW[i,:]=0
        #     sess.run(upOps[v], {upPH[v]: cW})
        #     cumAll[ind+1]=accuracy.eval({x: tx, y: ty})
        #     ind=ind+1
        #
        # plt.plot(cumAll)
        # plt.show()


################# SAVING STUFF in a json
        #get the Scope order
    cScope=getScope(model)
    order=[cScope,]
    todo=[model,]
    while todo:
        cNode=todo.pop()
        tScope=getScope(cNode)
        if (tScope!=cScope):
            if (tScope in scopes):
                order.append(tScope)
        cScope=tScope
        todo.extend(list(cNode.op.inputs))
    order=list(reversed(order))
    # order.append('output')
    print(order)

    outjson={}
    outjson['trainedAcc']=float(trainedAcc)
    outjson['scopes']=[]
    # outjson['network']=[{'index':i,'value':cumAll[i]} for i in range(len(cumAll))]
    # outjson['output']=[]
    for k in accuracies.keys():
        print(k)
        values=list(accuracies[k])#trainedAcc-
        valret=[]
        cummret=[{'index':0, 'value':float(trainedAcc)},]
        for i in range(len(values)):
            valret.append({'index':i,'value':values[i]})
        for i in range(len(cummulative[k])):
            cummret.append({'index':i+1,'value':cummulative[k][i]})

        outjson['scopes'].append({
            'name':k,
            'index':order.index(k.split('/')[0]),
            'numel':int(len(accuracies[k])),
            'values':valret,
            'cummulative':cummret,
            'removeOrder':[int(x) for x in removeOrder[k]]
        })

    with open('data.json','w') as f:
        f.write(json.dumps(outjson, sort_keys=True, indent=4))
