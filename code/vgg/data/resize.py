from glob import glob
from scipy.misc import imread, imresize, imsave

for img in glob('??.jpg'):
    imsave('s_'+img,imresize(imread(img,mode='RGB'),(224,224)))
