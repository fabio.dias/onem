import tensorflow as tf
from vgg16 import vgg16
from scipy.misc import imread, imresize
import numpy as np
from imagenet_classes import class_names

#from model import get_model, get_test, get_variables

def get_variables():
    with open('vars.list','r') as f:
        return([x for x in f.read().split('\n') if x!=''])

def get_model(x,sess):
    vgg = vgg16(x, 'vgg16_weights.npz', sess)
    return(vgg.probs)

def get_test():
    # 1-20 : jay (17)
    #21-40 : Canis Lupus (269)
    X=np.zeros((40,224,224,3))
    Y=np.zeros((40,1000))
    Y[:20,17]=1.0
    Y[20:,269]=1.0
    for i in range(40):
        X[i,:,:,:] = imread('./data/s_{:02d}.jpg'.format(i+1), mode='RGB')
    return(X,Y)

if __name__ == '__main__':
    sess = tf.Session()
    tx,ty=get_test()


    x = tf.placeholder(tf.float32, [None, 224, 224, 3], name='x')
    y = tf.placeholder("float", [None, ty.shape[1]],name='y')
    #vgg = vgg16(imgs, 'vgg16_weights.npz', sess)
    model=get_model(x,sess)
    prob = sess.run(model, feed_dict={x: tx})
    print(prob.shape)
    for i in range(prob.shape[0]):
        print(i)
        preds = (np.argsort(prob[i,:])[::-1])[0:3]
        print(preds)
        print(np.argmax(ty[i,:]))
        for p in preds:
            print(class_names[p], prob[i,p])
        print('--------------------------\n')
    correct_prediction = tf.equal(tf.argmax(model, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    trainedAcc=sess.run(accuracy,{x: tx, y: ty})
    print(trainedAcc)

#    prob = sess.run(vgg.probs, feed_dict={vgg.imgs: [img1]})[0]
#    preds = (np.argsort(prob)[::-1])[0:5]
#    for p in preds:
#        print(class_names[p], prob[p])
